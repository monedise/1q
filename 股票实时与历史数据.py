# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 22:57:23 2017

@author: Administrator
"""

import requests,re,os,pandas as pd,time

#Text函数是一个网页访问方法
def Text(url):
    try:
        r = requests.get(url)
        r.raise_for_status()
        
        r.encoding = "utf-8"
        return r.text
    except:
        return ""
def Text2(url):
    try:
        r = requests.get(url)
        r.raise_for_status()
        
        r.encoding = "GB2312"
        return r.text
    except:
        return "" 
def a股列表():
    lst=[]
    rrsh='http://www.p1234.com/gupiao/list_sh.html'
    rrsz='http://www.p1234.com/gupiao/list_sz.html'
    rrcyd='http://www.p1234.com/gupiao/list_cyb.html'
    listr='http://www.bestopview.com/stocklist.html'
    htmlsh = Text(rrsh)
    htmlsz = Text(rrsz)
    htmlcyd = Text(rrcyd)
    htmllistr=Text(listr)
    if htmlsh=="" and htmlsz=='' and htmlcyd=='':
        for t7 in re.findall(r'(<li><a href="http://www.bestopview.com/superView.php?stockcode=(.*?)" target="_blank">(.*?)</a></li>)', str(htmllistr)):
            if t7[1][0]=='6':
                lst.append('sh'+t7[1])
            else:
                lst.append('sz'+t7[1])
    else:
        for t in re.findall(r'(<DIV style="WIDTH: 160px; MARGIN-LEFT: 6px; float:left;" class=description><a href="/gupiao/(.*?)/">(.*?)</a>(.*?)</DIV></DIV>)', str(htmlsh)):
            lst.append('sh'+t[1])
        for t in re.findall(r'(<DIV style="WIDTH: 160px; MARGIN-LEFT: 6px; float:left;" class=description><a href="/gupiao/(.*?)/">(.*?)</a>(.*?)</DIV></DIV>)', str(htmlsz+r'\n'+htmlcyd)):
            lst.append('sz'+t[1])
    return lst

def 龙虎榜(jj):
    rr='http://vip.stock.finance.sina.com.cn/q/go.php/vLHBData/kind/ggtj/index.phtml?last='+str(jj)
    htmlkk=Text2(rr)
    ti=[]
    for t8 in re.findall(r'(<a class="page" href="(.*?)" onclick="set_page_num((.*?))">(.*?)</a>)', str(htmlkk)):
        ti.append(t8[4])
    fi=int(ti[-2])
    print(fi)
    for j in range(fi):
        ef=rr+'&p='+str(j+1)
        htmk=Text2(ef)
        w0=[]
        w1=[]
        w2=[]
        w3=[]
        w4=[]
        w5=[]
        w6=[]
        w7=[]
        for t1 in re.findall(r'(<td><a href="(.*?)" target="_blank">(.*?)</a></td>(.*?)<td><a href="(.*?)" target="_blank">(.*?)</a></td>(.*?)<td>(.*?)</td>(.*?)<td>(.*?)</td>(.*?)<td>(.*?)</td>(.*?)<td>(.*?)</td>(.*?)<td>(.*?)</td>(.*?)<td>(.*?)</td>)', str(htmk),re.S):
            w0.append(t1[2])
            w1.append(t1[5])
            w2.append(t1[7])
            w3.append(t1[9])
            w4.append(t1[11])
            w5.append(t1[13])
            w6.append(t1[15])
            w7.append(t1[17])      
    df = pd.DataFrame({'代码' : w0,
                       '名称' : w1,
                       '次数' : w2,
                       '累积购买额' : w3,
                       '累积卖出额' : w4,
                       '净额' :w5,
                       '买入席位数' : w6,
                       '卖出席位数' : w7})
    return df
    
        
def 所有实时():
    lst=a股列表()     
    sl = 'http://hq.sinajs.cn/list='
    w0=[]
    w1=[]
    w2=[]
    w3=[]
    w4=[]
    w5=[]
    w6=[]
    w7=[]
    w8=[]
    w9=[]
    w10=[]
    w11=[]
    w12=[]
    w13=[]
    w14=[]
    w15=[]
    w16=[]
    w17=[]
    w18=[]
    w19=[]
    w20=[]
    w21=[]
    w22=[]
    w23=[]
    w24=[]
    w25=[]
    w26=[]
    w27=[]
    w28=[]
    w29=[]
    w30=[]
    for stock in lst:
        r90= requests.get(sl + stock)
        try:
            for t in re.findall(r'(="(.*?)";)', str(r90.text)):
                w=t[1].split(',')
            if len(w)=='':
                continue
            w0.append(str(w[0]))
            w1.append(int(stock[2:]))
            w2.append(w[1])
            w3.append(float(w[2]))
            w4.append(float(w[3]))
            w5.append(float(w[4]))
            w6.append(float(w[5]))
            w7.append(int(w[8])/100)
            w8.append(float(w[9])/10000)
            w9.append(int(w[10])/100)
            w10.append(float(w[11]))
            w11.append(int(w[12])/100)
            w12.append(float(w[13]))
            w13.append(int(w[14])/100)
            w14.append(float(w[15]))
            w15.append(int(w[16])/100)
            w16.append(float(w[17]))
            w17.append(int(w[18])/100)
            w18.append(float(w[19]))
            w19.append(int(w[20])/100)
            w20.append(float(w[21]))
            w21.append(int(w[22])/100)
            w22.append(float(w[23]))
            w23.append(int(w[24])/100)
            w24.append(float(w[25]))
            w25.append(int(w[26])/100)
            w26.append(float(w[27]))
            w27.append(int(w[28])/100)
            w28.append(float(w[29]))
            w29.append(str(w[30]))
            w30.append(str(w[31]))
            df = pd.DataFrame({'股票名字':w0,'股票代码':w1,'今日开盘价':w2,'昨日收盘价':w3,'当前价格':w4,'今日最高价':w5,'今日最低价':w6,'成交量':w7,'成交额':w8,'买一/股':w9,'买一':w10,'买二/股':w11,'买二':w12,'买三/股':w13,'买三':w14,'买四/股':w15,'买四':w16,'买五/股':w17,'买五':w18,'卖一/股':w19,'卖一':w20,'卖二/股':w21,'卖二':w22,'卖三/股':w23,'卖三':w24,'卖四/股':w25,'卖四':w26,'卖五/股':w27,'卖五':w28,'日期':w29,'时间':w30})
        except:
            continue
        #df = pd.DataFrame({'股票名字':w0,'股票代码':w1,'今日开盘价':w2,'昨日收盘价':w3,'当前价格':w4,'今日最高价':w5,'今日最低价':w6,'成交量':w7,'成交额':w8,'买一/股':w9,'买一':w10,'买二/股':w11,'买二':w12,'买三/股':w13,'买三':w14,'买四/股':w15,'买四':w16,'买五/股':w17,'买五':w18,'卖一/股':w19,'卖一':w20,'卖二/股':w21,'卖二':w22,'卖三/股':w23,'卖三':w24,'卖四/股':w25,'卖四':w26,'卖五/股':w27,'卖五':w28,'日期':w29,'时间':w30})
    return df

def 个股实时(ff):
    if str(ff)[0]=='6':
        sl = 'http://hq.sinajs.cn/list='+'sh'+str(ff)
    else:
        sl = 'http://hq.sinajs.cn/list='+'sz'+str(ff)
    r90= requests.get(sl)
    
    w2=[]
    w3=[]
    w4=[]
    w5=[]
    w6=[]
    w7=[]
    w8=[]
    w9=[]
    w10=[]
    w11=[]
    w12=[]
    w13=[]
    w14=[]
    w15=[]
    w16=[]
    w17=[]
    w18=[]
    w19=[]
    w20=[]
    w21=[]
    w22=[]
    w23=[]
    w24=[]
    w25=[]
    w26=[]
    w27=[]
    w28=[]
    w29=[]
    w30=[]
    try:
        for t in re.findall(r'(="(.*?)";)', str(r90.text)):
            w=t[1].split(',')
        if len(w)=='':
            print()
        w2.append(w[1])
        w3.append(float(w[2]))
        w4.append(float(w[3]))
        w5.append(float(w[4]))
        w6.append(float(w[5]))
        w7.append(int(w[8])/100)
        w8.append(float(w[9])/10000)
        w9.append(int(w[10])/100)
        w10.append(float(w[11]))
        w11.append(int(w[12])/100)
        w12.append(float(w[13]))
        w13.append(int(w[14])/100)
        w14.append(float(w[15]))
        w15.append(int(w[16])/100)
        w16.append(float(w[17]))
        w17.append(int(w[18])/100)
        w18.append(float(w[19]))
        w19.append(int(w[20])/100)
        w20.append(float(w[21]))
        w21.append(int(w[22])/100)
        w22.append(float(w[23]))
        w23.append(int(w[24])/100)
        w24.append(float(w[25]))
        w25.append(int(w[26])/100)
        w26.append(float(w[27]))
        w27.append(int(w[28])/100)
        w28.append(float(w[29]))
        w29.append(str(w[30]))
        w30.append(str(w[31]))
        df= pd.DataFrame({'今日开盘价':w2,'昨日收盘价':w3,'当前价格':w4,'今日最高价':w5,'今日最低价':w6,'成交量':w7,'成交额':w8,'买一/股':w9,'买一':w10,'买二/股':w11,'买二':w12,'买三/股':w13,'买三':w14,'买四/股':w15,'买四':w16,'买五/股':w17,'买五':w18,'卖一/股':w19,'卖一':w20,'卖二/股':w21,'卖二':w22,'卖三/股':w23,'卖三':w24,'卖四/股':w25,'卖四':w26,'卖五/股':w27,'卖五':w28,'日期':w29,'时间':w30})
    except:
        print()
        #df = pd.DataFrame({'股票名字':w0,'股票代码':w1,'今日开盘价':w2,'昨日收盘价':w3,'当前价格':w4,'今日最高价':w5,'今日最低价':w6,'成交量':w7,'成交额':w8,'买一/股':w9,'买一':w10,'买二/股':w11,'买二':w12,'买三/股':w13,'买三':w14,'买四/股':w15,'买四':w16,'买五/股':w17,'买五':w18,'卖一/股':w19,'卖一':w20,'卖二/股':w21,'卖二':w22,'卖三/股':w23,'卖三':w24,'卖四/股':w25,'卖四':w26,'卖五/股':w27,'卖五':w28,'日期':w29,'时间':w30})
    return df
    
        
    
#这是一个历史数据函数 里面有两个时间参数 分别是开始与结束  
def 历史(起始=20150101,结束=(time.strftime("%Y%m%d"))):
    lst=a股列表()
    w2=[]
    w3=[]
    w4=[]
    w5=[]
    w6=[]
    w7=[]
    w8=[]
    w9=[]
    w10=[]
    w11=[]
    for stock in lst:
        pa='http://q.stock.sohu.com/hisHq?code=cn_'+stock[2:] +'&start='+str(起始)+'&end='+str(结束)
        htmlc = Text(pa)
        if htmlc=='{}':
            return False
        else:
            for tr in re.findall(r'("hq":\[\[(.*?)\]\],"code":)', str(htmlc)):
                for p in tr[1].split('],['):
                    try:
                        im = p.strip(',').split(',')
                        w2.append(im[0].replace('"',''))
                        w3.append(im[1].replace('"',''))
                        w4.append(im[2].replace('"',''))
                        w5.append(im[6].replace('"',''))
                        w6.append(im[5].replace('"',''))
                        w7.append(im[7].replace('"',''))
                        w8.append(im[8].replace('"',''))
                        w9.append(im[4].replace('"',''))
                        w10.append(im[3].replace('"',''))
                        w11.append(im[9].replace('"',''))
                        
                        df= pd.DataFrame({'日期':w2,'开盘价':w3,'收盘价':w4,'最高价':w5,'最低价':w6,'成交量':w7,'成交额':w8,'涨跌幅':w9,'涨跌额':w10,'换手率':w11})
                    except:
                        continue
    return df


def 分时数据(tt):
    ww=a股列表()
    mi = int(round(time.time()*1000))
    for e in ww:
        qq='http://money.finance.sina.com.cn/quotes_service/api/jsonp_v2.php/var%20_'+ e +'_'+tt+'_'+mi+'=/CN_MarketData.getKLineData?symbol='+e+'&scale='+tt'&ma=no&datalen=1023'


    
    
    
